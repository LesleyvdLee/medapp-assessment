abstract class Icons {
  static String alarm = 'baseline_alarm_black_24pt';
  static String business = 'baseline_business_center_black_24pt';
  static String bookmark = 'baseline_class_black_24pt';
  static String breakfast = 'baseline_free_breakfast_black_24pt';
  static String home = 'baseline_home_black_24pt';
  static String pharmacy = 'baseline_local_pharmacy_black_24pt';
}
