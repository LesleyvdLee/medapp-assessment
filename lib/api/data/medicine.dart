class Medicine {
  Medicine({
    required this.name,
    this.taken = false,
  });

  String name;
  bool taken;
}
