import 'package:medapp/api/data/medicine.dart';

class Moment {
  Moment({
    required this.title,
    required this.date,
    required this.icon,
    required this.medicines,
    this.isCollapsed = true,
  });

  String title;
  String icon;
  List<Medicine> medicines;
  DateTime date;
  bool isCollapsed;

  bool get isChecked {
    for (Medicine medicine in medicines) {
      if (medicine.taken == false) {
        return false;
      }
    }

    return true;
  }

  String get time {
    return '${date.hour.toString().padLeft(2, '0')}:${date.minute.toString().padLeft(2, '0')}';
  }
}
