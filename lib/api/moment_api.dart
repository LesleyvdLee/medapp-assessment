import 'dart:math';

import 'package:medapp/api/data/icons.dart';
import 'package:medapp/api/data/medicine.dart';
import 'package:medapp/api/data/moment.dart';

abstract class MomentApi {
  static final Random _random = Random();

  static DateTime _getDate(int day, int hour) {
    return DateTime(2022, 2, day, hour);
  }

  static Moment _breakfast(int day) {
    return Moment(
      title: 'Ontbijt',
      date: _getDate(day, 8),
      icon: Icons.breakfast,
      medicines: <Medicine>[
        Medicine(name: 'Paracetamol', taken: _random.nextBool()),
        Medicine(name: 'Vitamine C', taken: _random.nextBool()),
      ],
    );
  }

  static Moment _lunch(int day) {
    return Moment(
      title: 'Lunch',
      date: _getDate(day, 12),
      icon: Icons.home,
      medicines: <Medicine>[
        Medicine(name: 'Acebutol', taken: _random.nextBool()),
      ],
    );
  }

  static Moment _atWork(int day) {
    return Moment(
      title: 'Op \'t werk',
      date: _getDate(day, 15),
      icon: Icons.business,
      medicines: <Medicine>[
        Medicine(name: 'Paracetamol', taken: _random.nextBool()),
      ],
    );
  }

  static Moment _bedTime(int day) {
    return Moment(
      title: 'Voor het slapen',
      date: _getDate(day, 22),
      icon: Icons.alarm,
      medicines: <Medicine>[
        Medicine(name: 'Melatonin'),
      ],
    );
  }

  static Future<List<Moment>> getMoments() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 1000));

    final List<Moment> moments = <Moment>[
      _breakfast(1),
      _lunch(1),
      _breakfast(2),
      _lunch(2),
      _atWork(2),
      _breakfast(3),
      _lunch(3),
      _breakfast(4),
      _atWork(4),
      _breakfast(6),
      _lunch(6),
      _atWork(6),
      _bedTime(7),
      _breakfast(8),
      _lunch(8),
      _breakfast(9),
      _lunch(9),
      _atWork(9),
      _breakfast(10),
      _lunch(10),
      _breakfast(11),
      _atWork(11),
      _breakfast(13),
      _lunch(13),
      _atWork(13),
      _bedTime(14),
    ];

    moments[_random.nextInt(moments.length)].isCollapsed = false;

    return moments;
  }
}
