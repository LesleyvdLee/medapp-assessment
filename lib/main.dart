import 'package:flutter/material.dart';
import 'package:medapp/modules/home/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: const Color.fromRGBO(82, 135, 143, 1),
        colorScheme: ColorScheme(
          brightness: Brightness.light,
          primary: const Color.fromRGBO(82, 135, 143, 1),
          onPrimary: Colors.white,
          secondary: const Color.fromRGBO(242, 242, 242, 1),
          onSecondary: Colors.black,
          tertiary: const Color.fromRGBO(140, 195, 75, 1),
          error: Colors.red[900]!,
          onError: Colors.white,
          background: const Color.fromRGBO(230, 230, 230, 1),
          onBackground: Colors.white,
          surface: Colors.white,
          onSurface: Colors.black,
        ),
      ),
      initialRoute: '/home',
      routes: <String, Widget Function(BuildContext)>{
        '/home': (BuildContext context) =>
            const MyHomePage(title: 'MedApp Assessment'),
      },
    );
  }
}
