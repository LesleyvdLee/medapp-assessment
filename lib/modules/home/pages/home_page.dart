import 'package:flutter/material.dart';
import 'package:medapp/api/data/moment.dart';
import 'package:medapp/api/moment_api.dart';
import 'package:medapp/modules/home/screens/schedule.dart';
import 'package:medapp/modules/home/screens/taken.dart';
import 'package:provider/provider.dart';

class MomentsNotifier extends ChangeNotifier {
  List<Moment>? value;

  Future<void> getMoments() async {
    value = await MomentApi.getMoments();
    notifyListeners();
  }

  @override
  void dispose() {
    value = null;
    super.dispose();
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  MomentsNotifier moments = MomentsNotifier();
  late TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    moments.getMoments();
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    moments.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        bottom: TabBar(
          controller: tabController,
          indicatorColor: Colors.white,
          tabs: const <Tab>[
            Tab(
              text: 'Schedule',
            ),
            Tab(
              text: 'Taken',
            )
          ],
        ),
      ),
      body: RefreshIndicator(
        onRefresh: moments.getMoments,
        child: TabBarView(
          controller: tabController,
          children: <Widget>[
            ChangeNotifierProvider<MomentsNotifier>.value(
              value: moments,
              child: const ScheduleScreen(),
            ),
            ChangeNotifierProvider<MomentsNotifier>.value(
              value: moments,
              child: const TakenScreen(),
            ),
          ],
        ),
      ),
    );
  }
}
