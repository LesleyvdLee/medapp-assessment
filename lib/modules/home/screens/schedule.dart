import 'package:flutter/material.dart';
import 'package:medapp/modules/home/pages/home_page.dart';
import 'package:medapp/modules/home/widgets/moment_widget.dart';
import 'package:provider/provider.dart';

class ScheduleScreen extends StatefulWidget {
  const ScheduleScreen({Key? key}) : super(key: key);

  @override
  State<ScheduleScreen> createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  late MomentsNotifier moments;

  bool isSameDay(DateTime? previous, DateTime current) {
    if (previous == null) {
      return false;
    }

    return previous.year == current.year &&
        previous.month == current.month &&
        previous.day == current.day;
  }

  @override
  Widget build(BuildContext context) {
    moments = Provider.of<MomentsNotifier>(context);
    return ListView.builder(
      key: UniqueKey(),
      itemCount: moments.value?.length ?? 1,
      addAutomaticKeepAlives: false,
      itemBuilder: (_, int index) {
        if (moments.value == null) {
          return const Padding(
            padding: EdgeInsets.all(40),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        DateTime? _previous;
        if (index > 0) {
          _previous = moments.value![index - 1].date;
        }

        return MomentWidget(
          showDate: !isSameDay(_previous, moments.value![index].date),
          moment: moments.value![index],
          onChange: (_) {
            moments.value![index] = _;
          },
        );
      },
    );
  }
}
