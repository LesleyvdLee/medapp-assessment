import 'package:flutter/material.dart';
import 'package:medapp/api/data/medicine.dart';
import 'package:medapp/api/data/moment.dart';
import 'package:medapp/modules/home/pages/home_page.dart';
import 'package:provider/provider.dart';

class TakenScreen extends StatefulWidget {
  const TakenScreen({Key? key}) : super(key: key);

  @override
  State<TakenScreen> createState() => _TakenScreenState();
}

class _TakenScreenState extends State<TakenScreen> {
  late MomentsNotifier moments;

  int getTakenCount() {
    if (moments.value == null) {
      return 0;
    }

    int taken = 0;

    for (Moment moment in moments.value!) {
      for (Medicine medicine in moment.medicines) {
        if (medicine.taken) {
          taken++;
        }
      }
    }

    return taken;
  }

  @override
  Widget build(BuildContext context) {
    moments = Provider.of<MomentsNotifier>(context);
    return Padding(
      padding: const EdgeInsets.all(24),
      child: Center(
        child: Text(
          'You have taken ${getTakenCount()} medicines so far!',
          style: const TextStyle(fontSize: 30),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
