import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:medapp/api/data/medicine.dart';
import 'package:medapp/api/data/moment.dart';

class MomentWidget extends StatefulWidget {
  const MomentWidget({
    Key? key,
    required this.onChange,
    required this.moment,
    this.showDate = false,
  }) : super(key: key);

  final void Function(Moment) onChange;
  final Moment moment;
  final bool showDate;

  @override
  State<MomentWidget> createState() => _MomentWidgetState();
}

class _MomentWidgetState extends State<MomentWidget> {
  late Moment moment;

  @override
  void initState() {
    moment = widget.moment;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (widget.showDate)
          Padding(
            padding: const EdgeInsets.only(
              bottom: 12,
              top: 24,
              left: 24,
              right: 24,
            ),
            child: Text(DateFormat.MMMMEEEEd().format(moment.date)),
          ),
        Container(
          margin: const EdgeInsets.only(
            bottom: 6,
            top: 6,
            left: 24,
            right: 24,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 3,
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: ExpansionTile(
              key: UniqueKey(),
              maintainState: true,
              initiallyExpanded: !moment.isCollapsed,
              collapsedBackgroundColor: moment.isChecked
                  ? Theme.of(context).colorScheme.primary
                  : Colors.white,
              backgroundColor: moment.isChecked
                  ? Theme.of(context).colorScheme.primary
                  : Colors.white,
              title: Text(
                moment.title,
                style: TextStyle(
                  color: moment.isChecked ? Colors.white : Colors.black,
                ),
              ),
              subtitle: Text(
                moment.time,
                style: TextStyle(
                  color: moment.isChecked ? Colors.white : Colors.black,
                ),
              ),
              onExpansionChanged: (_) {
                moment.isCollapsed = !_;
              },
              children: moment.medicines.map((Medicine med) {
                return Container(
                  color: Colors.white,
                  child: ListTile(
                    title: Text(med.name),
                    trailing: GestureDetector(
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            med.taken = !med.taken;
                          });
                        }

                        widget.onChange(moment);
                      },
                      child: Image.asset(
                        'assets/${med.taken ? 'checkbox_checked_green.png' : 'checkbox_empty.png'}',
                        width: 24,
                        height: 24,
                      ),
                    ),
                  ),
                );
              }).toList(),
              trailing: GestureDetector(
                onTap: () {
                  if (mounted) {
                    setState(() {
                      for (Medicine _ in moment.medicines) {
                        if (moment.isChecked) {
                          _.taken = false;
                        } else {
                          _.taken = true;
                        }
                      }
                    });
                  }

                  widget.onChange(moment);
                },
                child: Image.asset(
                  'assets/${moment.isChecked ? 'checkbox_checked_white.png' : 'checkbox_empty.png'}',
                  width: 24,
                  height: 24,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
